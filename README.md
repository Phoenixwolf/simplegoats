# Simple Goats
This is a simple CSS theme for Voat subverses.

Just go into your moderator section and replace the CSS with what is in the
simplegoats.css file.

You can see it at the [Simplegoats Subverse](https://voat.co/v/simplegoats) 
or in the pictures below.
It works in both light and dark mode and mobile. All versions can be edited
separately. 

I have added what I call snips, which are just that. They are snippets of CSS
that you can add to the end of your own CSS and increase the functionality
of what you have. I will be adding more of these so that it will be
easier for you to just add and customize. That is essentially what
Simplegoats is, just a bunch of snippets put together to create a
larger effect. 

You can add these snips to any CSS code for subverses, they do not depend on each
other or simplegoats.css and each one adds something different. The simplegoats.css
includes all of the snips, but some may be commented out because I don't use them
on the subverse.

If you get an error when you add the code, use the minified (simplegoats.min.css) version, as the main
.css file could possibly be too long. 

Maybe one day I will get to a point that I will make the Readme more...readable.

Light Theme:

![](https://slimgur.com/images/2015/07/15/66dd900010e16d63a18db8adfcbbb09a.md.png)

Dark Theme:

![](https://slimgur.com/images/2015/07/15/c13ff428dc822bd710dc5be82cd1816c.md.png)

